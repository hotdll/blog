<p align="center"><a href="#" target="_blank" rel="noopener noreferrer"><img width="100" src="_media/logo.jpg" alt="Luat logo"></a></p>

## LuaTask_V3.0
合宙 2G/4G/CAT.1 通用 lib 库

1. 支持 2G/4G/CAT.1 模块, lib库通用
2. 串口支持流模式和帧模式
3. Socket 支持帧模式和流模式
4. Uart、Socket、MQTT、WebSocket支持同步和异步模式

----------------------------------------------------------------------------------

## 一个透传代码，只需要4行代码：

```lua
-- 创建 uart 对象
local u1 = uart.new(1, 115200)
-- 创建 webSocket 对象
local sc = socket.new("tcp")
sys.taskInit(sc.start, sc, "180.97.80.55", "12415", 180, nil, nil, 25, "heart", function(msg)u1:send(msg) end)
sys.taskInit(u1.start, u1, nil, 10, nil,  function(msg)sc:send(msg) end)
```

## 如何使用LuaTask V3.0

1. 首先, 有个开发板([LTE](https://luat.taobao.com/))
2. 然后, 下载[固件包](https://gitee.com/hotdll/LuaTask_V3.0)
3. 然后, 按教程下载和调试[LuaTools](https://hotdll.gitee.io/blog)
4. 开始愉快地玩耍(或者放着积灰...)

----------------------------------------------------------------------------------
## 资源

* 参阅 [Luat 平台层](docs/markdown/core/luat_platform.md)
* [Lua 5.3中文手册](https://www.runoob.com/manual/lua53doc/)
* [合宙官网](http://www.openluat.com)
* [合宙商城](http://m.openluat.com)
* [Lua API文档](https://wiki.openluat.com/)

----------------------------------------------------------------------------------

## 使用到的开源项目

* [lua](https://www.lua.org/) Lua官网
* [X-MagicBox](https://gitee.com/hotdll/magic_box) 在RT-Thread环境下读取ds18b20
* [LuaTask](https://github.com/openLuat/Luat_2G_RDA_8955) 合宙LuaTask
* [iRTU](https://gitee.com/hotdll/iRTU) 基于Luat的DTU, 稀饭大神
* [elua](http://www.eluaproject.net/) eLua 虽然已经停更多年,但精神犹在

## 更多项目

* [iRTU](https://gitee.com/hotdll/iRTU) 开源DTU/RTU解决方案
* [Luat_CSDK_Air724U](https://gitee.com/openLuat/Luat_CSDK_Air724U) 市面上最畅销的4G Cat.1模块的开发包
* [llcom](https://github.com/chenxuuu/llcom) 可运行lua脚本的高自由度串口调试工具
* [irtu-gps](https://gitee.com/wendal/irtu-gps) 基于iRTU项目,实现GPS数据的接收和展示


## 总体架构

![总体架构](_media/coreLua.jpg)

## 授权协议

[MIT License](LICENSE)
