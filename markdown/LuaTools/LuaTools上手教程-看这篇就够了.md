## LuaTools 上手教程

By : 稀饭放姜 （2020/10/20）

### LuaTools 功能

1. 在 LuaTask 开发方式下，查看合宙模块的运行日志：![1603163088120](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603163088120.png)
2. 在 AT 开发模式下，显示简单的 AT 开机信息：![1603163344309](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603163344309.png)
3. LuaTask 开发协助抓底层日志（选项-工具配置）：![1603163725333](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603163725333.png)
4. LuaTask 开发项目管理（下载，更新，打包固件，工程管理）：![1603164041559](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164041559.png)
5. 打包后的固件下载（core 或 AT 或 core+lib+usr三合一 固件）： ![1603164057775](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164057775.png)
6. 其他帮助功能:![1603164113428](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164113428.png)![1603164136083](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164136083.png)

### LuaTools 下载

1. 官网下载：http://www.openluat.com/Product/Index.html
2. 直接下载：http://www.openluat.com/Product/file/luatoolsV2-redirect.html

### LuaTools 安装

1. LuaTask V2 版本下载后的文件名为：Luatools_v2.exe
2. 在win10系统--D盘（或其它盘D\E\F\G\H\...）根目录下新建文件夹并重命名为：LuaTools![1603164573803](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164573803.png)
3. 将下载的Luatools_v2.exe 拷贝或移动到 新建的 LuaTools文件夹内：![1603164784942](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164784942.png)
4. 点Luatools_v2.exe 右键发送快捷方式到桌面即可:![1603164860731](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164860731.png)
5. 双击 Luatools_v2.exe 启动程序并更新，如果有杀软拦截，请务必通过，请务必通过，请务必通过，重要的事情说三遍，特别的软件开发请勿用xxx安全全家桶。![1603164973942](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603164973942.png)
6. 更新完成后，LuaTools 文件夹下会生成几个文件夹：![1603165008904](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603165008904.png)

### LuaTools 各个文件夹功能：

1. _temp : 临时文件。
2. config : 对Luatools_V2 进行的一些配置会存放到这个文件夹。
3. log ： 这个文件夹和我们关系紧密，当你向合宙 FAE 寻求技术支持的时候，需要提交这个目录下的文件。
4. resource ： 合宙模块相关的AT固件，LuaTask 开发的Core 底层固件，Lib库 和demo
5. project ： 使用项目管理后会自动生成这个目录，用来管理下载项目的：![1603165284546](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603165284546.png)

### LuaTools 主界面功能：

1. 账户：![1603165337659](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603165337659.png)

	- 登陆： 用购买时销售给你自动创建的erp账号登陆，可以在 FAE 远程支持的时候协助抓取本机 Log。
	- 流量卡管理平台：链接到： http://sim.openluat.com/login
	- Luat 物联平台：远程升级和debug时用，链接到：http://iot.openluat.com/Login/
	- 经纬度查询：开发基站定位时会用到，链接到：http://bs.openluat.com/bs
	- 模块生产信息查询：查询模块出厂信息时用，链接到：http://imei.openluat.com/login
	- Luat 短信平台：物联网卡短信平台，具体购买使用联系销售。
2. 设备操作

- 主要是利用 LuaTools V2 自带的 AT固件来帮助用户将模块一键切换到对应的AT固件。

3. 选项

	- 工具配置，配合 FAE 抓取bug日志时使用，如使用Luatoos V2 经查遇到蓝屏，可以先关闭底层日志的抓取：![1603165783212](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603165783212.png)

4. 帮助
![1603165848368](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603165848368.png)

	- 远程支持和请求远程支持 请先联系销售得到指定的FAE支持后由FAE协助使用这两项功能，平时用不上。
	- 查询模块生产记录：
    ![1603165985257](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603165985257.png)
    - wiki 教程：LuaTask API， 链接到： https://wiki.openluat.com/
    - 资料下载：模块全部资料都在官网，链接到：http://www.openluat.com/Product/
    - 技术支持QQ群：
      - 合宙Luat通信模块（2G） 1 群：604902189（已满）
      - 合宙Luat通信模块（2G） 2 群：423804427
      - 合宙Luat通信模块（2G） 3 群：201848376
      - 合宙Luat(Cat.1模块) 1 群：1092305811
      - 合宙Luat(cat.1模块) 2 群：877164555
      - 合宙Luat(Cat.4模块) 1 群：851800257（已满）
      - 合宙Luat(Cat.4模块）2 群：387396364
      - LuatOS技术交流群：1061642968
      - 合宙Luat(NB-IoT模块)：627242078
      - Luat C-SDK开发者交流：151645843
      - 合宙Luat北斗GPS模块：967368887
      - Luat模块AT指令交流群：710412579
      - 合宙Luat(SmartDTU) 1 群： 952343033 （已满）
      - 合宙Luat(SmartDTU) 2 群： 1027923658

5. 2G 和 4G 打印日志选择窗口：
   
    -  ![1603167001548](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603167001548.png)
    -  4G 模块支持 USB （默认）打印和模块 Uart 物理串口打印日志
    -  2G 模块支持 Host （默认）打印和模块 Uart 物理串口打印日志
    -  在非 USB 打印时，可以选择与模块Uart物理串口相连的 USB - TTL工具在 windows 设备管理器对应的端口号,如：
![1603167240359](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603167240359.png)    
    ![1603167323876](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603167323876.png)
    -  在非 USB 打印时，可以选择打开，关闭串口或者重启串口，以排除串口无响应的问题。
    -  开始打印 （停止打印） 可以占停文本区的打印自动滚动
    -  清除打印 可以清空文本群的打印日志，但是不会清空 Luatools 下Log目录下的文件内容。
    -  重启 2G 模块，在 2G 模块调试时，方便软件重启 2G 模块。
    
6. 模块状态简易显示窗口

    **注意：这里显示的状态是从滚动文本群抓取关键词提取的，如果日志不完整，简易状态是不准的！！！！**![1603167841998](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603167841998.png)

    - 启动原因：大部分时候无法显示，2G 模块可以日志中搜索 poweron
    - 系统状态：如果滚动文本区中存在对应的日志，这里会滚动显示SIM卡、网络注册等信息
    - 固件版本：这里仅仅显示core 的版本信息，例如AT或者Luat的版本信息，不显示Lib和用户脚本的版本号
    - 信号强度：这里显示GSM的信号强度；特别地，信号强度与卡是否欠费无关，不插卡也可以有信号强度
    - 软件类型：这里是指 Core 的类型，例如Luatask 或者 AT 

7. 下载固件入口

    - ![1603168096536](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603168096536.png)
    - ![1603168135890](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603168135890.png)

8. 项目管理入口

    - ![1603168156136](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603168156136.png)
    - ![1603168192357](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603168192357.png)

9. 搜索打印

    - ![1603168240573](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603168240573.png)

10. 底部链接

    - ![1603168260596](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603168260596.png)
    - 特别地，软件右下角有个三角符号，表示软件窗口可以拖动大小。

### LuaTools  驱动部分

LuaTools 只是日志和下载管理工具，日志的显示和固件的下载依赖驱动的支持，以下以官方的开发板为例。如果是你自己开发的板子，2G的以你实际手里的 USB-TTL 为准。4G/CAT.1 模块可以参考开发板例子。

#### LuaTools 驱动下载

1. 2G 开发板驱动地址：https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers
2. 4G 开发板驱动地址：http://www.openluat.com/Product/file/asr1802/AirM2M_USB_Download&Com_Driver_for_4G_V1.0.0.4.7z
3. Cat.1 开发板驱动地址：http://www.openluat.com/Product/file/uis8910dm/sw_file_20200303181718_8910_module_usb_driver_signed%20_20200303_hezhou.7z

#### LuaTools 驱动安装：

1. 2G 按照默认一路安装即可。
2. 4G 开发板（Air720/D/H/U 和 Air720SL/SD/SH/SG/SGI）需要按照这个教程（禁用驱动签名）安装：
   1. 文字教程：http://ask.openluat.com/article/463/0
   2. 视频教程：https://www.bilibili.com/video/BV1Q441187gG?p=4
3. Cat.1开发板（Air720U/G/H Air724U/G/H Air820xx) 的驱动已经通过微软认证，正常安装即可，如果碰到无法下载（设备被识别为：ELMO GMAS），需要手工安装：
   1. 正常的 Cat.1 模块 重启下载时候设备管理：![1603170137771](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170137771.png)
   2. windows 错误识别成其他驱动的情况：![1603170209133](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170209133.png)
   3. 手工安装方法：
      1. 点ELMO GMAS 设备右键 -- 更新驱动程序![1603170234194](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170234194.png)
      2. 选择 浏览我的计算机以查找驱动程序软件![1603170333321](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170333321.png)
      3. 浏览到下载解压后的 Cat.1 驱动目录![1603170447924](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170447924.png)![1603170424272](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170424272.png)
      4. 单击下一步，即可正确识别![1603170477983](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170477983.png)
      5. 如果 打印的三个虚拟串口无法显示，也可以用上面的访法，逐个安装。

### LuaTools 日志查看 

日志查看使用的熟练度取决于使用者的实际编程经验和水平，这里仅仅解释下日志（滚动文本区）基本的日志：

1. 使用 LuaTools 滚动文本区查看日志：

   如下图：

   ![1603170802587](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603170802587.png)

   - [2020-10-20 13:14:12.867] : 日志时间，注意：这里的时间是windows的时间，并非模块打印日志的准确时间。
   - [I] : LuaTask  打印日志的标志，I 表示 info 级别，E 表示 ERROR 级别，其他级别参考 log.lua 的 api 。
   - -[ril.proatc] +CSQ: 21,99 ： 对应的库或者用户自己打印的日志信息。
   - 如果缺少 [i] 这一栏的，说明日志可能是底层日志，也可能是用户没使用log.lua日志功能。

2. 使用文本编辑器直接查看 log 日志： 

   LuaTools 的日志是 USB-TTL 或者 USB-VCOM 提供的，因此可能会独占串口，用文本编辑器打开log的时候，请注意生成的 Log  文件时间，必要时，先关闭 LuaTools 再使用文本编辑器查看日志：

   - ![1603171179389](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603171179389.png)
   - ![1603171253413](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603171253413.png)
### LuaTools 项目管理

在使用 LuaTools 下载源码时，强烈建议使用项目管理来进行下载，每个下载都用一个项目来区分。

#### 新建项目

1. 创建项目：![1603171587916](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603171587916.png)
2. 输入 TestSocket 然后确定：![1603171692362](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603171692362.png)
3. 双击 TestSocket （激活该项目），在右侧分别填入：
   1. core ： LuaTask 开发的底层Core
   2. 添加 Core 后，增加脚本 按钮可用，然后添加 用户脚本，用户数据，以及lib库。
   3. 默认 USB 打印Trace(日志)， Trace 三个选项是用来选择打印输出方式的，上文有讲，这里是配置的地方。
   4. 注意：！！！！ 此处强烈建议不要勾选：添加默认lib
   5. 升级文件包含core 【项目打包用】按需选择
   6. 升级文件包含脚本 【项目打包用】【勾选】
   7. 升级文件无资源文件 【项目打包用】【不选】
   8. USB BOOT 下载：使用 Boot 模式下载（下载项目会讲）
   9. 下载脚本 ： 只下载 【脚本列表】中的数据
   10. 下载脚本和底层： 下载【底层core】和 【脚本列表】中的数据
   11. 语法检查：单独检查用户编写的脚本有无语法错误
4. **注意！！！：首次下载强烈建议使用 --  【下载脚本和底层】下载。**

#### 下载项目

LuaTools 使用的是Python + wxPython 编写，意味着他的实际下载调用的是原厂提供的 API 或者工具库实现，故Luatools V2 仅仅支持 64 位 windows 系统，强烈建议使用 win10 64 位系统，以获取最佳使用体验。

1. 2G 模块只要能用 HOST 口打印日志，即可下载，唯一要注意的是波特率高达921600Bps，对用户的USB-TTL和线材质量要求较高，无法下载，多次重启模块无效的，请检查USB-TTL和数据线是否满足需求。

2. 开发板的功能按钮介绍：
	- **这里特地说明下：主板的开机方式为打开电源开关（ON）长按开机键2秒松手。** 
![1603173030736](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603173030736.png)
	1. 电源开关： ON 给主板供电，OFF 给主板断电
	2. 重   启： 重启按钮
	3. 开   机： 长按2秒开机
	4.   BOOT： 按住 BOOT 开机 或者开机后按 BOOT不松手，然后按 重启按键，即可进入下载 BOOT 下载模式 

3. Air720/720S/724U/720U 下载的条件和两种方式
   1. 正常情况下，windows 设备管理器会出现 类似下图：![1603172508224](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603172508224.png)
   2. 这三个虚拟串口是用来调试和打印日志使用的，不是下载用。
   3. 下载使用的是这个串口，这个串口在下载或者进入 Boot 模式的时候可见：
    - Cat.1（Air720u/724u） ![1603172673465](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603172673465.png)
    - Cat.4 (Air720S)
      ![1603172899398](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603172899398.png)

4. 如果模块无法开机，每次长按开机键开机后，设备管理器都是上图显示的样子，那就是设备进 BOOT 模式了，重新按BOOT模式下载即可。

##### USB BOOT 模式下载

1. 开发板【电源开关】-- OFF 
2. 如下图已经配置好的 TestSocket 项目：![1603173704477](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603173704477.png)
3. 要注意勾选：【USB BOOT 下载】
4. 更换底层或者首次下载选【下载底层和脚本】，修改脚本可以选【下载脚本】：![1603173870111](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603173870111.png)
5. 开发板开发板【电源开关】-- ON
6. 按住【BOOT】按键
7. 按住【开机】按键 2秒 松手
8. 提示 “可松开USB_BOOT，正在下载....” 时即可松开BOOT按键：![1603173971346](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603173971346.png)
9. **下载过程请勿断电或者断开数据线**：![1603174004629](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603174004629.png)
10. 如果中间出现异常，按上面流程重新操作即可。

##### 调试模式下载

在大部分情况下，可以不用 USB BOOT 模式下载，这样调试脚本的时候更方便。

1. 按正常操作，打开【电源开关】--ON，长按2秒开机键开机。

2. 确保 LuaTools 的USB打印日志正常，如果不能打印日志自能用 USB BOOT 模式下载
3. 按下图配置好项目：![1603174308431](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603174308431.png)
4. 点下载脚本，LuaTools 会自动重启模块，并进入下载模式：![1603174346010](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603174346010.png)
5. **如果下载失败，尝试两次无果后，请使用 USB BOOT 模式下载。**

##### 下载AT固件或打包好的固件

下载 AT 固件 或者打包好 core+lib+usr_script 的固件也分 USB BOOT 模式 和  普通调试模式。推荐选择BOOT模式：

![1603174481725](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603174481725.png)

1. 选择文件：

    - 2G 是lod文件，例如：AirM2M_V5984_AT_S_TTS.lod
    - Air720S (ASR1802S)系列是 .blf ,例如：AirM2M_720S_V405_LTE_AT.blf
    - Air720U(RDA8910, Cat.1)系列是.pac ，例如：AirM2M_720U_V838_LTE_AT.pac
2. 选择 USB BOOT 下载：![1603174857594](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603174857594.png)
3. 点下载，然后等：![1603174960360](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603174960360.png)
4. 上电开机或者重启模块（如果启用了USB BOOT，则按住BOOT 开机或者重启）![1603174989753](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603174989753.png)
5. 等待下载完成：
    ![1603175029891](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603175029891.png)
6. 如果下载失败，多尝试几次，或者先关机，然后再重复上面流程。

#### 打包项目

打包项目是将底层core 和 Luat 的lib库，以及 用户写的脚本件和用户数据文件一起打包到.lod(.blf 或.pac)文件中，方便量产或者发给别人。在项目管理中的下面部分，既是项目打包的设置：![1603175215671](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603175215671.png)

1. 量产文件路径：可以自定义目录，默认是 LuaTools 的目录下（.），：![1603175333317](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603175333317.png)

2. 生成量产文件：如果前面的下载调试都没问题，点：【生成量产文件】按钮即可在Luatools/4G量产文件/下生成打包后的文件：![1603175424568](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603175424568.png)

3. 加密脚本：用来保护源码的，最长16位长度

4. 生成文件增加后缀：用来区别同名固件的，一般用不上。

5. 升级文件（.bin) 的打包

   在生成量产文件的时候，即可生成升级文件，升级文件扩展名是.bin。![1603175507413](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/1603175507413.png)

   .bin 文件是用来生成远程升级文件（合宙提供免费iot.openluat.com 用来升级固件）。

#### 打包升级文件注意事项：

1. Cat.1（Air720U/Air724U）系列，打包的时候要勾选![image-20201020165931748](LuaTools%E4%B8%8A%E6%89%8B%E6%95%99%E7%A8%8B-%E7%9C%8B%E8%BF%99%E7%AF%87%E5%B0%B1%E5%A4%9F%E4%BA%86.assets/image-20201020165931748.png)
2. Cat.4 （ASR1802S-Air720S/SL/SD/SG/SH）完整升级的时，需要勾选【包含core】
3. Cat.4 （ASR1802S-Air720S/SL/SD/SG/SH）只升级脚本时，不用需要勾选【包含core】